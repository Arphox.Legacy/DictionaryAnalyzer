﻿using DictionaryAnalyzer.Core.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DictionaryAnalyzer.WpfVisualizer
{
    /// <summary>
    ///     A helper class to easily manage the visualizer WPF app
    /// </summary>
    public sealed class AppManager : IDisposable
    {
        /// <summary>
        ///     Singleton instance of the <see cref="AppManager"/> class.
        /// </summary>
        public static AppManager Instance = new AppManager();

        private App application = null;
        private bool isDisposed = false;
        private bool isInitialized = false;

        private AppManager() { }

        /// <summary>
        ///     Initializes a new WPF App.
        /// </summary>
        /// <remarks>
        ///     Warning: this method is NOT thread safe.
        /// </remarks>
        private void Initialize()
        {
            if (isDisposed)
                throw new ObjectDisposedException(nameof(AppManager), $"Cannot reuse the disposed {nameof(AppManager)}.");
            if (isInitialized)
                throw new InvalidOperationException($"{nameof(Initialize)} method can only be called once.");

            bool isInitializing = true;

            var thread = new Thread(() =>
            {
                application = new App();
                application.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
                application.Startup += (sender, args) => { isInitializing = false; };
                application.Run();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Name = "AppManager_WpfAppHostThread";
            thread.Start();

            while (isInitializing) // waiting for App to be initialized
                Thread.Sleep(10);

            isInitialized = true;
        }

        /// <summary>
        ///     Starts a new <see cref="MainWindow"/> window.
        ///     The first call to this method will initialize the WPF <see cref="App"/>.
        /// </summary>
        /// <remarks>
        ///     Since <see cref="Initialize"/> method is NOT thread safe, this is not thread safe either.
        /// </remarks>
        public void Start(IDictionaryStateExtractor dictionaryStateExtractor)
        {
            if (isDisposed)
                throw new ObjectDisposedException(nameof(AppManager), $"Cannot reuse the disposed {nameof(AppManager)}.");
            if (!isInitialized)
                Initialize();

            application.Dispatcher.Invoke(() => new MainWindow(dictionaryStateExtractor).Show());
        }

        /// <summary>
        ///     Disposes the WPF application irreversibly, freeing up a thread.
        ///     After calling this method, you will not be able to call <see cref="Start"/> again.
        /// </summary>
        public void Dispose()
        {
            isDisposed = true;
            application.Dispatcher.Invoke(() => application.Shutdown());
        }
    }
}