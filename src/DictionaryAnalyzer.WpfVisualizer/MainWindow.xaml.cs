﻿using DictionaryAnalyzer.Core.Interfaces;
using System;
using System.Windows;
using System.Windows.Threading;

namespace DictionaryAnalyzer.WpfVisualizer
{
    public partial class MainWindow : Window
    {
        public MainWindow(IDictionaryStateExtractor dictionaryStateExtractor)
        {
            InitializeComponent();

            InitializeBucketListView(dictionaryStateExtractor);
        }

        private void InitializeBucketListView(IDictionaryStateExtractor dictionaryStateExtractor)
        {
            var bucketsListView = new Views.BucketsLinkedListView();
            maingrid.Children.Add(bucketsListView);

            DispatcherTimer pollingTimer = new DispatcherTimer();
            pollingTimer.Interval = TimeSpan.FromMilliseconds(500);
            pollingTimer.Tick += (sender, e) =>
            {
                bucketsListView.UpdateState(dictionaryStateExtractor.ExtractState());
            };

            pollingTimer.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }
    }
}