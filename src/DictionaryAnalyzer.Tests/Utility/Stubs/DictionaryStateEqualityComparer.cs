﻿using DictionaryAnalyzer.Core.Interfaces;

namespace DictionaryAnalyzer.Tests.Utility.Stubs
{
    public sealed class DictionaryStateEqualityComparer : IDictionaryStateEqualityComparer
    {
        public static DictionaryStateEqualityComparer AlwaysTrueInstance = new DictionaryStateEqualityComparer(true);
        public static DictionaryStateEqualityComparer AlwaysFalseInstance = new DictionaryStateEqualityComparer(false);

        private readonly bool staticAnswer;

        private DictionaryStateEqualityComparer(bool staticAnswer)
        {
            this.staticAnswer = staticAnswer;
        }

        public bool AreEqual(IDictionaryState x, IDictionaryState y) => staticAnswer;
    }
}