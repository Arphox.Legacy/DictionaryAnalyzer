﻿using DictionaryAnalyzer.Core.Watcher;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DictionaryAnalyzer.Tests.UnitTests
{
    /// <summary>
    ///     Unit tests for <see cref="TaskManager"/>
    /// </summary>
    [TestFixture]
    public sealed class TaskManagerTests
    {
        private static readonly Action EmptyAction = () => { };

        [Test]
        public void Start_CallsAction()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(5));

            // Act
            int callCounter = 0;
            taskManager.Start(() => { callCounter++; });
            Thread.Sleep(100);
            taskManager.Stop();

            // Assert
            Assert.That(callCounter, Is.AtLeast(1));
        }

        [Test]
        public void Start_CallsActionMultipleTimes()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(5));

            // Act
            int callCounter = 0;
            taskManager.Start(() => { callCounter++; });
            Thread.Sleep(100);
            taskManager.Stop();

            // Assert
            Assert.That(callCounter, Is.AtLeast(2));
        }

        [Test]
        public void Start_DoesNotCallAction_TooManyTimes()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(20));

            // Act
            int callCounter = 0;
            taskManager.Start(() => { callCounter++; });
            Thread.Sleep(119);
            taskManager.Stop();

            // Assert
            Assert.That(callCounter, Is.AtMost(5)); // 119 / 20 = 5
        }

        [Test]
        public void ImmediateCallToStopAfterStart_DoesNotCallAction()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(10));

            // Act
            int callCounter = 0;
            taskManager.Start(() => { callCounter++; });
            taskManager.Stop();

            // Assert
            Assert.That(callCounter, Is.Zero);
        }

        [Test]
        public void Start_WhenMultipleCallsWithoutStop_ThrowsException()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(10));

            // Act
            taskManager.Start(EmptyAction);

            // Assert
            Assert.Throws<InvalidOperationException>(() => taskManager.Start(EmptyAction));
        }

        [Test]
        public void RestartTest()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(50));

            // Act
            taskManager.Start(EmptyAction);
            taskManager.Stop();

            // Assert
            Assert.DoesNotThrow(() => taskManager.Start(EmptyAction));
        }

        [Test]
        public void MultiRestartTest()
        {
            // Arrange
            var taskManager = new TaskManager(TimeSpan.FromMilliseconds(20));

            // Act & Assert
            taskManager.Start(EmptyAction);
            taskManager.Stop();

            for (int i = 0; i < 5; i++)
            {
                Assert.DoesNotThrow(() => taskManager.Start(EmptyAction));
                taskManager.Stop();
            }
        }
    }
}