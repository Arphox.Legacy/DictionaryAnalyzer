﻿using DictionaryAnalyzer.Core.StronglyTypedStateExtractor;
using DictionaryAnalyzer.WpfVisualizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DictionaryAnalyzer.ConsoleTester
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            //dict[1] = "A";
            //dict[8] = "B";
            //dict[2] = "C";
            //dict[10] = "D";
            //dict[17] = "E";
            //dict[15] = "F";
            //dict[24] = "G";

            var extractor = new StronglyTypedDictionaryStateExtractor<int, string>(dict);
            AppManager.Instance.Start(extractor);

            Thread.Sleep(1000);

            while (true)
            {
                dict[random.Next(0, 1000)] = RandomString(3);
                Thread.Sleep(random.Next(0, 1000));
            }


            Console.ReadLine();
            //Demo();
        }
        
        private static void Demo()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>(10);
            PrintState(dict);
            Console.ReadLine();

            for (int i = 0; i < 100; i += 2)
            {
                dict[$"{i}{i}{i}"] = i;
                PrintState(dict);
                Console.ReadLine();
            }

            Console.ReadLine();
        }

        private static void PrintState<TKey, TValue>(Dictionary<TKey, TValue> dict)
        {
            Console.Clear();

            StronglyTypedDictionaryState<TKey, TValue> state = new StronglyTypedDictionaryStateExtractor<TKey, TValue>(dict).ExtractState();
            Console.WriteLine($"Lengths = {state.Entries.Length}&{state.Buckets?.Length.ToString() ?? "null"}, " +
                $"Count = {state.Count}, FreeCount = {state.FreeCount}, FreeList = {state.FreeList}");

            Console.WriteLine("[index] | Bucket | EntryKey -> EntryValue  Next  HashCode      mod");
            int maxBucket = state.Buckets?.Max() ?? 0;
            int upperLimit = state.Buckets?.Length ?? 0;
            for (int i = 0; i < upperLimit; i++)
            {
                string index = $"[{i}]".PadRight(11);
                string bucket = state.Buckets[i].ToString().PadRight(8, ' ');
                if (state.Buckets[i] == -1)
                    bucket = "".PadRight(8, ' ');

                var entry = state.Entries[i];
                var key = entry.Key?.ToString().PadRight(9) ?? new string(' ', 9);
                var value = entry.Value.ToString().PadRight(11) ?? new string(' ', 11);
                var next = entry.Next.ToString().PadRight(6);
                var hash = entry.HashCode.ToString().PadRight(13);
                Console.WriteLine($"{index}{bucket}{key}->  {value}{next}{hash} {entry.HashCode % state.Entries.Length}");
            }
        }

        private static readonly Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}