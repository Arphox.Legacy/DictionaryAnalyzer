﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace DictionaryAnalyzer.Core.DynamicStateExtractor
{
    [DebuggerDisplay("{Key} -> {Value}, Next: {Next}, Hash: {HashCode}")]
    [StructLayout(LayoutKind.Auto)]
    public struct DynamicDictionaryEntry : IDictionaryEntry
    {
        public int HashCode { get; }
        public int Next { get; }
        public object Key { get; }
        public object Value { get; }

        public DynamicDictionaryEntry(int hashCode, int next, object key, object value)
        {
            HashCode = hashCode;
            Next = next;
            Key = key;
            Value = value;
        }
    }
}