﻿using DictionaryAnalyzer.Core.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DictionaryAnalyzer.Core.DynamicStateExtractor
{
    public sealed class DynamicDictionaryStateExtractor : IDictionaryStateExtractor<DynamicDictionaryState>
    {
        private readonly IDictionary dictionary;
        private readonly Type dictionaryType;

        public DynamicDictionaryStateExtractor(IDictionary dictionary)
        {
            this.dictionary = dictionary;
            dictionaryType = dictionary.GetType();

            if (!Helpers.IsGenericDictionary(dictionary))
                throw new ArgumentException("The parameter has to be a Dictionary<TKey, TValue>.", nameof(dictionary));
        }

        public DynamicDictionaryState ExtractState()
        {
            var convertedEntries = ConvertEntries();

            int count = GetFieldValue<int>(dictionary, "count");
            int freeList = GetFieldValue<int>(dictionary, "freeList");
            int freeCount = GetFieldValue<int>(dictionary, "freeCount");
            int[] buckets = GetFieldValue<int[]>(dictionary, "buckets");

            return new DynamicDictionaryState(buckets, convertedEntries.ToArray(), count, freeList, freeCount);
        }

        IDictionaryState IDictionaryStateExtractor.ExtractState()
        {
            return ExtractState();
        }

        private List<DynamicDictionaryEntry> ConvertEntries()
        {
            IEnumerable entries = GetEntries();
            if (entries == null)
                return null;

            var convertedEntries = new List<DynamicDictionaryEntry>();

            foreach (var entry in entries)
            {
                int hashCode = GetFieldValue<int>(entry, "hashCode");
                int next = GetFieldValue<int>(entry, "next");
                object key = GetFieldValue<object>(entry, "key");
                object value = GetFieldValue<object>(entry, "value");

                DynamicDictionaryEntry convertedEntry = new DynamicDictionaryEntry(hashCode, next, key, value);
                convertedEntries.Add(convertedEntry);
            }

            return convertedEntries;
        }

        private IEnumerable GetEntries()
        {
            Type type = dictionary.GetType();
            FieldInfo field = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).First(fi => fi.Name == "entries");
            return (IEnumerable)field.GetValue(dictionary);
        }

        private TField GetFieldValue<TField>(object obj, string fieldName)
        {
            // TODO: try boosting performance with cached FieldInfo?
            //       or by any other ways.

            var fi = obj.GetType().GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            return (TField)fi.GetValue(obj);
        }
    }
}