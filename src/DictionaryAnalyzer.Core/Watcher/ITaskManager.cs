﻿using System;

namespace DictionaryAnalyzer.Core.Watcher
{
    public interface ITaskManager : IDisposable
    {
        void Start(Action tickAction);
        void Stop();
    }
}