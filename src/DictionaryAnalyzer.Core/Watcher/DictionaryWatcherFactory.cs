﻿using DictionaryAnalyzer.Core.DynamicStateExtractor;
using DictionaryAnalyzer.Core.StronglyTypedStateExtractor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DictionaryAnalyzer.Core.Watcher
{
    public class DictionaryWatcherFactory
    {
        public DictionaryWatcher<DynamicDictionaryState> Create(
            object watchedDictionary, 
            CompareOptions compareOptions,
            TimeSpan pollingRate)
        {
            Check(watchedDictionary, compareOptions);
            EnsureGenericDictionary(watchedDictionary);

            var stateExtractor = new DynamicDictionaryStateExtractor((IDictionary)watchedDictionary);
            var stateComparer = DictionaryStateComparerFactory.Create(compareOptions);
            var taskManager = new TaskManager(pollingRate);
            return new DictionaryWatcher<DynamicDictionaryState>(stateExtractor, stateComparer, taskManager);
        }

        public DictionaryWatcher<StronglyTypedDictionaryState<TKey, TValue>> Create<TKey, TValue>(
            Dictionary<TKey, TValue> watchedDictionary,
            CompareOptions compareOptions,
            TimeSpan pollingRate)
        {
            Check(watchedDictionary, compareOptions);

            var stateExtractor = new StronglyTypedDictionaryStateExtractor<TKey, TValue>(watchedDictionary);
            var stateComparer = DictionaryStateComparerFactory.Create(compareOptions);
            var taskManager = new TaskManager(pollingRate);
            return new DictionaryWatcher<StronglyTypedDictionaryState<TKey, TValue>>(stateExtractor, stateComparer, taskManager);
        }

        private static void Check(object watchedDictionary, CompareOptions compareOptions)
        {
            if (watchedDictionary == null)
                throw new ArgumentNullException(nameof(watchedDictionary));
            if (!Enum.IsDefined(typeof(CompareOptions), compareOptions))
                throw new ArgumentOutOfRangeException(nameof(compareOptions));
        }

        private static void EnsureGenericDictionary(object watchedDictionary)
        {
            if (!Helpers.IsGenericDictionary(watchedDictionary))
                throw new ArgumentException($"The {nameof(watchedDictionary)} has to be a System.Collections.Generic.Dictionary<TKey,TValue>.");
        }
    }
}