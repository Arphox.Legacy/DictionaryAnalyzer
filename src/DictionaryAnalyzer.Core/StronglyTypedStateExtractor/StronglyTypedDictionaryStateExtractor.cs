﻿using DictionaryAnalyzer.Core.Interfaces;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DictionaryAnalyzer.Core.StronglyTypedStateExtractor
{
    public sealed class StronglyTypedDictionaryStateExtractor<TKey, TValue> : IDictionaryStateExtractor<StronglyTypedDictionaryState<TKey, TValue>>
    {
        private readonly Func<int[]> GetBuckets;
        private readonly Func<int> GetCount;
        private readonly Func<int> GetFreeList;
        private readonly Func<int> GetFreeCount;
        private readonly Func<IEnumerable> GetEntries;
        private readonly Func<object, int> GetEntryHashCode;
        private readonly Func<object, int> GetEntryNext;
        private readonly Func<object, TKey> GetEntryKey;
        private readonly Func<object, TValue> GetEntryValue;
        private readonly Dictionary<TKey, TValue> dictionary;

        public StronglyTypedDictionaryStateExtractor(Dictionary<TKey, TValue> dictionary)
        {
            var bucketsFieldAccessor = CreateFieldAccessor<Dictionary<TKey, TValue>, int[]>("buckets");
            GetBuckets = () => bucketsFieldAccessor(dictionary);

            var getCountAccessor = CreateFieldAccessor<Dictionary<TKey, TValue>, int>("count");
            GetCount = () => getCountAccessor(dictionary);

            var freeListAccessor = CreateFieldAccessor<Dictionary<TKey, TValue>, int>("freeList");
            GetFreeList = () => freeListAccessor(dictionary);

            var freeCountAccessor = CreateFieldAccessor<Dictionary<TKey, TValue>, int>("freeCount");
            GetFreeCount = () => freeCountAccessor(dictionary);

            var entriesAccessor = CreateFieldAccessor<Dictionary<TKey, TValue>, IEnumerable>("entries");
            GetEntries = () => entriesAccessor(dictionary);

            var entryHashCodeAccessor = CreateDynamicFieldAccessor("hashCode");
            GetEntryHashCode = obj => (int)entryHashCodeAccessor(obj);

            var entryNextAccessor = CreateDynamicFieldAccessor("next");
            GetEntryNext = obj => (int)entryNextAccessor(obj);

            var entryKeyAccessor = CreateDynamicFieldAccessor("key");
            GetEntryKey = obj => (TKey)entryKeyAccessor(obj);

            var entryValueAccessor = CreateDynamicFieldAccessor("value");
            GetEntryValue = obj => (TValue)entryValueAccessor(obj);

            this.dictionary = dictionary;
        }

        public StronglyTypedDictionaryState<TKey, TValue> ExtractState()
        {
            var convertedEntries = GetConvertedEntries();

            int count = GetCount();
            int freeList = GetFreeList();
            int freeCount = GetFreeCount();
            int[] buckets = GetBuckets();
            return new StronglyTypedDictionaryState<TKey, TValue>(buckets, convertedEntries.ToArray(), count, freeList, freeCount);
        }

        IDictionaryState IDictionaryStateExtractor.ExtractState()
        {
            return ExtractState();
        }

        private List<StronglyTypedDictionaryEntry<TKey, TValue>> GetConvertedEntries()
        {
            IEnumerable entries = GetEntries();

            // TODO: this should be null if entries is null as well to be consistent
            var convertedEntries = new List<StronglyTypedDictionaryEntry<TKey, TValue>>();

            if (entries == null)
                return convertedEntries;

            foreach (var entry in entries)
            {
                int hashCode = GetEntryHashCode(entry);
                int next = GetEntryNext(entry);
                TKey key = GetEntryKey(entry);
                TValue value = GetEntryValue(entry);

                StronglyTypedDictionaryEntry<TKey, TValue> convertedEntry = new StronglyTypedDictionaryEntry<TKey, TValue>(
                    hashCode,
                    next,
                    key,
                    value);

                convertedEntries.Add(convertedEntry);
            }

            return convertedEntries;
        }

        private static Func<TObj, TField> CreateFieldAccessor<TObj, TField>(string fieldName)
        {
            ParameterExpression param = Expression.Parameter(typeof(TObj), "arg");
            MemberExpression member = Expression.Field(param, fieldName);
            LambdaExpression lambda = Expression.Lambda(typeof(Func<TObj, TField>), member, param);
            Func<TObj, TField> compiled = (Func<TObj, TField>)lambda.Compile();
            return compiled;
        }

        private static Func<dynamic, object> CreateDynamicFieldAccessor(string fieldName)
        {
            ParameterExpression target = Expression.Parameter(typeof(object), "target");
            ParameterExpression result = Expression.Parameter(typeof(object), "result");

            CallSiteBinder getName = Binder.GetMember(
               CSharpBinderFlags.None,
               fieldName,
               typeof(Dictionary<TKey, TValue>), // So it will "see" the Dictionary's private Entry struct
               new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null) }
            );

            BlockExpression expr = Expression.Block(
                new[] { result },
                Expression.Assign(
                    result,
                    Expression.Dynamic(getName, typeof(object), target)
                )
            );

            Func<dynamic, object> compiled = Expression.Lambda<Func<dynamic, object>>(expr, target).Compile();
            return compiled;
        }
    }
}